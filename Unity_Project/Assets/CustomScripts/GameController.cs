using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
	// Module controllers
	public HUDController hudController { get; private set; }

	public ReasonController reasonController{ get; private set; }

	public SettingsController settingsCont{ get; private set; }

	public GameObject reasonObject;
	public GameObject hudObject;
	public GameObject gameOverObject;
	public GameObject photos;

	public delegate void EventHandler();

	public GameObject scorePopup;

	public static event EventHandler OnNextRound;
	public static event EventHandler OnEndSession;
	public static event EventHandler OnQuitGame;
	public static event EventHandler OnStartSession;
	// Holds the total number of rounds
	public int totalRounds{ get; private set; }
	// Holds the current round number
	public int currentRound { get; private set; }

	// Holds the emotions that are active
	private string[] emotions = null;

	public string[] thisSessionEmotions { get; private set; }

	// Random number generator
	private System.Random rnd = new System.Random();

	// SCore Controller
	public ScoreController scrCont = null;

	void Awake()
	{
		currentRound = 0;
		hudController = GetComponentInChildren<HUDController>();
		reasonController = GetComponentInChildren<ReasonController>();
		hudController.gameObject.SetActive(false);
		reasonController.gameObject.SetActive(false);
	}

	// Use this for initialization
	void Start()
	{
		// Set up image loader
		ImageLoader.LoadImages();

		// Get the menu system
		GameObject menuSys = GameObject.FindGameObjectWithTag("MenuController");
		MenuSystemController menuCont = menuSys.GetComponent<MenuSystemController>();
		// Get the settings screen
		settingsCont = menuCont.settingsScreen;

		scrCont = new ScoreController();
	}

	// Called on a correct answer
	public void CorrectAnswer()
	{
		int thisScore = scrCont.roundEnd(1);
		hudController.score += thisScore;

		GameObject score = Instantiate(scorePopup);
		score.GetComponent<UILabel>().text = "+" + thisScore;
		score.GetComponent<UILabel>().color = Color.green;

		Invoke("ShowNextRound", 2);
	}

	// Called on an incorrect answer
	public void IncorrectAnswer()
	{
		hudController.triesRemaining--;

		// TODO: check remaining tries
		if (hudController.triesRemaining == 0) {

			hudController.score += scrCont.roundEnd(0);
			ShowNextRound();
		} 
	}

	// Called to move to the next level
	public void ShowNextRound()
	{

		// Disable module for activation of new module
		reasonController.gameObject.SetActive(false);

		// Only go to the next level if there is another round to be played
		if (currentRound < settingsCont.totalRounds) {

			currentRound++;
			// Difficulty set to 0
			// TODO: implement difficulty algorithm
			int difficulty = scrCont.getDifficulty();

			// Set up the current round with random
			string curEmo = thisSessionEmotions [currentRound - 1];

			reasonController.setUpRound(0, curEmo);

			// Set up the HUD for the current round
			if (difficulty == 0)
				hudController.updateNewLevel(0, 2, currentRound, settingsCont.totalRounds);
			else
				hudController.updateNewLevel(0, 1, currentRound, settingsCont.totalRounds);

			// Set the level to display
			reasonController.gameObject.SetActive(true);

			scrCont.roundStart();

			if (OnNextRound != null)
				OnNextRound();

			//System.Threading.Thread.Sleep(1000);
		} else {
			hudObject.SetActive(false);
			reasonObject.SetActive(false);
			photos.SetActive(false);

			gameOverObject.GetComponent<GameOverController>().setFinalScore(hudController.score);
			gameOverObject.SetActive(true);


		}
	}

	private void QuitApplication()
	{
		if (OnQuitGame != null)
			OnQuitGame();
		Application.Quit();
	}

	public void StartGame()
	{
		hudController.gameObject.SetActive(true);
		reasonController.gameObject.SetActive(true);

		// holds all the available emotions (taken from settings)
		emotions = settingsCont.getActiveEmotions();
		totalRounds = settingsCont.totalRounds;

		// Creates the list of emotions for this game
		thisSessionEmotions = new string[totalRounds];
		for (int j = 0; j < totalRounds; j++)
			thisSessionEmotions [j] = emotions [rnd.Next(emotions.Length)];

		if (OnStartSession != null)
			OnStartSession();

		// Starts game on HUD
		scrCont.GameStart();

		ShowNextRound();
	}

	public void EndSession()
	{
		hudObject.SetActive(true);
		reasonObject.SetActive(true);
		photos.SetActive(true);
		gameOverObject.SetActive(false);

		hudController.gameObject.SetActive(false);
		reasonController.gameObject.SetActive(false);
		currentRound = 0;
		hudController.score = 0;
		hudController.currentRound = currentRound;

		scrCont = new ScoreController();
		int num = scrCont.getMultiplier();
		if (OnEndSession != null)
			OnEndSession();
	}

	void OnEnable()
	{
		MainMenuScreen.OnPlayButtonPressed += StartGame;
	}

	void OnDisable()
	{
		MainMenuScreen.OnPlayButtonPressed -= StartGame;
	}
}

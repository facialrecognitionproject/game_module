﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	private string theName = "New Player";

	public string playerName {
		get {
			return theName;
		}
		set {
			theName = value;
			Debug.Log ("Player name set as : " + theName);
		}
	}

}

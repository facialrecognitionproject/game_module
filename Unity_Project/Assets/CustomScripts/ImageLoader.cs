﻿using UnityEngine;
using System.Collections;

public class ImageLoader
{

	public enum EmotionType
	{
		HAPPY,
		SURPRISED,
		SAD,
		ANGRY,
		CONTENT
	}

	public static Object[] happyFaces{ get; private set; }

	public static Object[] surprisedFaces{ get; private set; }

	public static Object[] sadFaces{ get; private set; }

	public static Object[] angryFaces{ get; private set; }

	public static Object[] contentFaces{ get; private set; }

	private static bool initialized = false;

	static int prevHappy, prevSad, prevContent, prevSurprised,prevAngry = int.MaxValue;

	public static void LoadImages()
	{

		happyFaces = Resources.LoadAll("Happy", typeof(Sprite));
		surprisedFaces = Resources.LoadAll("Surprised", typeof(Sprite));
		sadFaces = Resources.LoadAll("Sad", typeof(Sprite));
		angryFaces = Resources.LoadAll("Angry", typeof(Sprite));
		contentFaces = Resources.LoadAll("Content", typeof(Sprite));


		initialized = true;
	}

	public static Sprite GetRandomFace(EmotionType emotion)
	{
		if (!initialized) {
			Debug.LogError("ImageLoader: Images not loaded. ImageLoaded must be initialized before getting a face.");
			return null;
		}
		System.Random rnd = new System.Random();
		int nextIndex;

		switch (emotion) {

			case EmotionType.HAPPY:
				nextIndex = rnd.Next(happyFaces.Length);
				while(nextIndex == prevHappy)
					nextIndex = rnd.Next(happyFaces.Length);
				prevHappy = nextIndex;
				return (Sprite)happyFaces [nextIndex];
			case EmotionType.SURPRISED:
				nextIndex = rnd.Next(surprisedFaces.Length);
				while(nextIndex == prevSurprised)
					nextIndex = rnd.Next(surprisedFaces.Length);
				prevSurprised = nextIndex;
				return (Sprite)surprisedFaces [nextIndex];
			case EmotionType.SAD:
				nextIndex = rnd.Next(sadFaces.Length);
				while(nextIndex == prevSad)
					nextIndex = rnd.Next(sadFaces.Length);
				prevSad = nextIndex;
				return (Sprite)sadFaces [nextIndex];
			case EmotionType.ANGRY:
				nextIndex = rnd.Next(angryFaces.Length);
				while(nextIndex == prevAngry)
					nextIndex = rnd.Next(angryFaces.Length);
				prevAngry = nextIndex;
				return (Sprite)angryFaces [nextIndex];
			case EmotionType.CONTENT:
				nextIndex = rnd.Next(contentFaces.Length);
				while(nextIndex == prevContent)
					nextIndex = rnd.Next(contentFaces.Length);
				prevContent = nextIndex;
				return (Sprite)contentFaces [nextIndex];
		}
		return null;
	}

	public static void LoadSummary()
	{
	
		Debug.Log("Load summary:" +
			"\nHappy Faces:\t" + happyFaces.Length +
			"\nSurprised Faces:\t" + surprisedFaces.Length +
			"\nSad Faces:\t" + sadFaces.Length +
			"\nAngry Faces:\t" + angryFaces.Length +
			"\nContent Faces:\t" + contentFaces.Length);
	}
}

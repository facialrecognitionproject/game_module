﻿using UnityEngine;
using System.Collections;

public class HUDControllerTest : MonoBehaviour
{

	private HUDController hud;

	void Start()
	{
		CreateMock();
	}

	bool done = false;

	void Update()
	{
		if (!done)
			DoTests();
		done = true;
	}

	private void CreateMock()
	{
		hud = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().hudController;
	}

	private void DoTests()
	{
		TestUtils.assert("Current Round is zero", hud.currentRound == 0);
		hud.currentRound++;
		TestUtils.assert("Increment Current Round", hud.currentRound == 1);
		hud.difficulty = 0;
		TestUtils.assert("Difficulty is zero", hud.difficulty == 0);
		hud.difficulty++;
		TestUtils.assert("Difficulty is incremented", hud.difficulty == 1);
		hud.totalRounds = 10;
		TestUtils.assert("Total Rounds is set", hud.totalRounds == 10);
		hud.multiplier = 1;
		TestUtils.assert("Multiplier is 1", hud.multiplier == 1);
		hud.multiplier++;
		TestUtils.assert("Multiplier is incremented", hud.multiplier == 2);
		hud.multiplier = -1;
		TestUtils.assert("Multiplier is clamped at lower-bound of 1", hud.multiplier == 1);
		hud.score = 0;
		TestUtils.assert("Score is zero", hud.score == 0);
		hud.score++;
		TestUtils.assert("Score is incremented", hud.score == 1);
		hud.score = -5;
		TestUtils.assert("Score is clamped at lower-bound of zero", hud.score == 0);
		hud.triesRemaining = 5;
		TestUtils.assert("Tries is set", hud.triesRemaining == 5);
		hud.triesRemaining--;
		TestUtils.assert("Tries is decremented", hud.triesRemaining == 4);
		hud.triesRemaining++;
		TestUtils.assert("Tries is incremented", hud.triesRemaining == 5);
		hud.triesRemaining = -1;
		TestUtils.assert("Tries is clamped at a lower-bound of zero", hud.triesRemaining == 0);
		hud.triesRemaining = 1;
		hud.triesRemaining = 0;
		TestUtils.assert("Tries zero-case", hud.triesRemaining == 0);
	}
}

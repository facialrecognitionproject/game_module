﻿using UnityEngine;
using System.Collections;

public class TestUtils : MonoBehaviour
{

	public static void assert(string failureMessage, bool value)
	{
		if (!value)
			Debug.LogError("TestUtils: " + failureMessage + ": failed!");
	}

}

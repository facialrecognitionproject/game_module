﻿using UnityEngine;
using System.Collections;

public class ImageLoaderTest : MonoBehaviour
{

	void Awake()
	{
	
		ImageLoader.LoadImages();
	}

	// Use this for initialization
	void Start()
	{
		TestUtils.assert("Angry face not loaded", ImageLoader.GetRandomFace(ImageLoader.EmotionType.ANGRY) != null);
		
		TestUtils.assert("Surprised face not loaded", ImageLoader.GetRandomFace(ImageLoader.EmotionType.SURPRISED) != null);
		
		TestUtils.assert("Content face not loaded", ImageLoader.GetRandomFace(ImageLoader.EmotionType.CONTENT) != null);
		
		TestUtils.assert("Happy face not loaded", ImageLoader.GetRandomFace(ImageLoader.EmotionType.HAPPY) != null);
		
		TestUtils.assert("Sad face not loaded", ImageLoader.GetRandomFace(ImageLoader.EmotionType.SAD) != null);
		ImageLoader.LoadSummary();
	}
}

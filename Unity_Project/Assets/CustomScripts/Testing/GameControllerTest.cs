﻿using UnityEngine;
using System.Collections;

public class GameControllerTest : MonoBehaviour
{
	private GameController gameCont = null;
	private GameObject mockObject = null;

	public void DoTests()
	{
		// Check round set to 0 initially
		TestUtils.assert("Test current round is zero", gameCont.currentRound == 0);
		// Test settings screen rounds == game controller rounds
		TestUtils.assert("Total rounds == settings screen", gameCont.totalRounds == 
			GameObject.FindGameObjectWithTag("MenuController")
		                 .GetComponent<MenuSystemController>()
		                 .settingsScreen.totalRounds);

		gameCont.CorrectAnswer();
		TestUtils.assert("Correct answer increases round #", gameCont.currentRound == 1);
	}

	private void CreateMock()
	{
		mockObject = GameObject.FindGameObjectWithTag("GameController");
		gameCont = mockObject.GetComponent<GameController>();
	}

	bool done = false;

	void Awake()
	{
		CreateMock();
	}

	void LateUpdate()
	{
		if (!done) {
			DoTests();
			done = true;
		}
	}
}

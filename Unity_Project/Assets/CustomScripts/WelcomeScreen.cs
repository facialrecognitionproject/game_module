﻿using UnityEngine;
using System.Collections;

public class WelcomeScreen : MonoBehaviour
{
	private UILabel welcomeMessage;

	void Awake ()
	{
		welcomeMessage = transform.FindChild ("WelcomeMessage").GetComponent<UILabel> ();
	}

	void OnEnable ()
	{
		welcomeMessage.text = "Welcome ";
		GameObject playerObj = GameObject.FindGameObjectWithTag ("Player");
		welcomeMessage.text += playerObj.GetComponent<Player> ().playerName + "!";
	}
}

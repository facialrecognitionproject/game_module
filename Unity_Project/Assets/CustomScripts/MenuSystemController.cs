﻿using UnityEngine;
using System.Collections;

public class MenuSystemController : MonoBehaviour
{
	// The script controlling the welcome screen
	public WelcomeScreen welcomeScreen { get; private set; }
	// The script controlling the sign in screen
	public SignInScreen signInScreen { get; private set; }
	// The script controlling the main menu screen
	public MainMenuScreen mainMenuScreen { get; private set; }
	// The script controlling the settings screen
	public SettingsController settingsScreen { get; private set; }

	void Awake()
	{
		welcomeScreen = menuObjects [0].GetComponent<WelcomeScreen>();
		signInScreen = menuObjects [1].GetComponent<SignInScreen>();
		mainMenuScreen = menuObjects [2].GetComponent<MainMenuScreen>();
		settingsScreen = menuObjects [3].GetComponent<SettingsController>();
	}

	void Start()
	{
		// Set all screens to inactive
		CloseMenus();
		// Set the first screen to be the welcome screen
		activeScreen = Screens.WelcomeScreen;
	}

	// Open the main menu
	public void OpenMainMenu()
	{
		activeScreen = Screens.MainMenuScreen;
	}

	// Open the welcome screen
	public void OpenWelcomeScreen()
	{
		activeScreen = Screens.WelcomeScreen;
	}

	// Open the sign in screen
	public void OpenSignInScreen()
	{
		activeScreen = Screens.SignInScreen;
	}

	public void OpenSettingScreen()
	{
		activeScreen = Screens.SettingsScreen;
	}

	public void CloseMenus()
	{
		foreach (GameObject go in menuObjects) {
			if (go.activeInHierarchy)
				go.SetActive(false);
		}
	}

	// The different values the state machine can take
	private enum Screens
	{
		WelcomeScreen,
		SignInScreen,
		SettingsScreen,
		MainMenuScreen
	}

	// The currently active state
	private Screens currScreen;
	// The game objects corresponding to different menu screens
	public GameObject[] menuObjects;
	// Property to change states/screen
	private Screens activeScreen {
		set {
			if (currScreen != null)
				ExitState(currScreen);
			currScreen = value;
			EnterState(value);
		}
	}

	// Enter state function of the state machine to perform
	// any actions associated with a new state
	private void EnterState(Screens state)
	{
		switch (state) {
			case Screens.WelcomeScreen:
				menuObjects [0].SetActive(true);
				break;
			case Screens.SignInScreen:
				menuObjects [1].SetActive(true);
				break;
			case Screens.MainMenuScreen:
				menuObjects [2].SetActive(true);
				break;
			case Screens.SettingsScreen:
				menuObjects [3].SetActive(true);
				break;
		}
	}

	// Exit state function of the state machine to perform any 
	// actions required before transitioning states
	private void ExitState(Screens oldState)
	{
		switch (oldState) {
			case Screens.WelcomeScreen:
				menuObjects [0].SetActive(false);
				break;
			case Screens.SignInScreen:
				menuObjects [1].SetActive(false);
				break;
			case Screens.MainMenuScreen:
				menuObjects [2].SetActive(false);
				break;
			case Screens.SettingsScreen:
				menuObjects [3].SetActive(false);
				break;
		}
	}

	void OnEnable()
	{
		MainMenuScreen.OnPlayButtonPressed += CloseMenus;
		MainMenuScreen.OnSettingsButtonPressed += OpenSettingScreen;
		GameController.OnEndSession += OpenMainMenu;
	}

	void OnDisable()
	{
		MainMenuScreen.OnPlayButtonPressed -= CloseMenus;
		MainMenuScreen.OnSettingsButtonPressed -= OpenSettingScreen;
		GameController.OnEndSession -= OpenMainMenu;
	}
}

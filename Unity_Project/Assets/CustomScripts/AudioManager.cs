﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{
	public AudioClip[] musicTracks;
	public float blendFactor = .5f;
	private AudioClip currentClip = null;
	private AudioClip nextClip = null;
	private bool blending = false;
	private int index = 0;
	private AudioSource source = null;
	private bool playMusic = false;

	public void ToggleMusic()
	{
		playMusic = !playMusic;
		if (playMusic && source != null)
			clip = musicTracks [0];
		else if (!playMusic && source.isPlaying)
			source.Stop();
	}

	public AudioClip clip {
		get {
			return currentClip;
		}
		set {
			if (playMusic && !blending) {
				nextClip = value;
				StartCoroutine("BlendTracks");
			}
		}
	}

	void Awake()
	{
		gameObject.AddComponent<AudioSource>();
		source = this.GetComponent<AudioSource>();
		if (musicTracks.Length > 1) {
			source.clip = musicTracks [0];
			nextClip = musicTracks [1 % musicTracks.Length];
		}
	}

	void Start()
	{
		// Get whether music should be played
		playMusic = GameObject.FindGameObjectWithTag("MenuController")
			.GetComponent<MenuSystemController>().settingsScreen.GetMusicSetting();
		if (playMusic)
			this.GetComponent<AudioSource>().Play();

	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space) && Application.isEditor)
			this.PlayNextTrack();

		if (!source.isPlaying)
			PlayNextTrack();
	}

	public void PlayNextTrack()
	{
		index = (index + 1) % musicTracks.Length;
		clip = musicTracks [index];
	}

	public void PlayRandomClip()
	{
		// TODO Implement this
	}

	private IEnumerator BlendTracks()
	{
		AudioSource source = this.GetComponent<AudioSource>();
		bool done = false;
		float defVolume = source.volume;
		blending = true;
		while (!done) {

			if (source.clip != nextClip) {
				if (source.volume > 0)
					source.volume -= Time.deltaTime * blendFactor;
				else {
					source.volume = 0;
					source.Stop();
					source.clip = nextClip;
					currentClip = nextClip;
					source.Play();
				}
			} else {
				source.volume += Time.deltaTime * blendFactor;
				if (source.volume >= defVolume) {
					source.volume = defVolume;
					break;
				}
			}
			yield return null;
		}
		blending = false;
	}
}

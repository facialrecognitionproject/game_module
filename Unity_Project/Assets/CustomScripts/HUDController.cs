using UnityEngine;
using System.Collections;

public class HUDController : MonoBehaviour
{
	private string answer;
	private int diff;
	// Property corresponding to the rounds difficulty
	public int difficulty {
		get {
			return diff;
		}
		set {
			// Don't allow negative numbers
			diff = value < 0 ? 0 : value;
			UpdateTextOnComponent("Difficulty", "Difficulty: " + difficulty);
		}
	}

	// The current score
	private int curScore = 0;
	// Property corresponding to the users score
	public int score {
		get {
			return curScore;
		}
		set {
			// Don't allow negative numbers
			curScore = value < 0 ? 0 : value;
			UpdateTextOnComponent("Score", "Score: " + score);
		}
	}

	private int tries;
	// Property corresponding to the number of tries remaining
	public int triesRemaining {
		get {
			return tries;
		}
		set {
			tries = value >= 0 ? value : 0;
			UpdateTextOnComponent("TriesRemaining", "Tries Remaining: " + triesRemaining);
		}
	}

	private int totRds = 0;
	// Property corresponding to total number of rounds
	public int totalRounds {
		get {
			return totRds;
		}
		set {
			totRds = value;
			UpdateTextOnComponent("RoundNumber", "Round " + currentRound + " of " + totalRounds);
		}
	}

	private int currRd = 0;
	// Property corresponding to the current round we're on
	public int currentRound {
		get {
			return currRd;
		}
		set {
			currRd = value;
			UpdateTextOnComponent("RoundNumber", "Round " + currentRound + " of " + totalRounds);
		}
	}

	private int mult = 1;
	// Property corresponding to the current multiplier
	public int multiplier {
		get {
			return mult;
		}
		set {
			// Clamp multiplier at 1
			mult = value >= 1 ? value : 1;
			UpdateTextOnComponent("GameMultiplier", "x" + mult + " Multiplier");
		}
	}

	// Helper function to assist in updating text on a component
	private void UpdateTextOnComponent(string componentName, string text)
	{
		transform.FindChild(componentName).GetComponent<UILabel>().text = text;
	}

	public void updateNewLevel(int difficulty, int numTries, int levelNumber, int totalLevels)
	{
		this.difficulty = difficulty;
		this.triesRemaining = numTries;
		this.currentRound = levelNumber;
		this.totalRounds = totalLevels;
	}
}

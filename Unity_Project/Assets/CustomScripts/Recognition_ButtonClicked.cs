﻿using UnityEngine;
using System.Collections;

public class Recognition_ButtonClicked : MonoBehaviour {

	public UILabel bttnLabel;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnClick() {
		GameObject img = GameObject.Find ("MMEmotionImage");
		SceneImageController imgController = img.GetComponentInChildren<SceneImageController> ();
		
		if (imgController.isCorrect (this.bttnLabel.text)) {
			imgController.pickEmotion();
		} else {
			gameObject.GetComponentInChildren<UIButton> ().defaultColor = Color.red;
		}
	}
}

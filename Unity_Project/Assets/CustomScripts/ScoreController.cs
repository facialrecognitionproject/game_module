
using UnityEngine;
using System;
using System.Diagnostics;

public class ScoreController
	{

		private int scoreTotal;
		private int scoreRound;
		private int difficulty;
		private int sLoss;
		private int sWin;
		private double pSkill;
		private Stopwatch timer;

		public const int EASY = 0;
		public const int MEDIUM = 1;
		public const int HARD = 2;


		public ScoreController()
		{
			timer = new Stopwatch();
			scoreTotal = 0;
			scoreRound = 0;
			sWin 	= 0;
			sLoss	= 0;
			pSkill  = 0;
			difficulty = EASY;
		}

		public void GameStart(){
			scoreTotal = 0;
			scoreRound = 0;
			sWin 	= 0;
			sLoss	= 0;
			if (pSkill < 33)
				difficulty = EASY;
			else if (pSkill >= 33 && pSkill <= 66)
				difficulty = MEDIUM;
			else {
				difficulty = HARD;
			}
		}

		public void roundStart(){
			timer.Reset();
			timer.Start();
		}

		public int roundEnd(int win){
			timer.Stop();

			if (win == 1) {
				sWin++;
				sLoss = 0;
			} else {
				sLoss++;
				sWin = 0;
			}

			TimeSpan ts = timer.Elapsed;

			long time = ts.Milliseconds;
			if (time > 5000)
				time = 5001;
			double td = (double)time;

			double dL = (((double)win * Math.Log((double)sWin + .01) + 1) * (15000.0 - Math.Min(td*2,10000.0))/5000.0)
			- (0.8 * (double)(1 - win) * (Math.Log((double)sLoss + .01) + 1));

			pSkill += dL;
			if (pSkill < 0)
				pSkill = 0;
			if (pSkill > 100)
				pSkill = 100;

			int scoreChange = (int)(Math.Round((Math.Max(dL, 0) + 1) * (difficulty + 1)*100));

			scoreRound = scoreChange;
			scoreTotal += scoreChange;

			if (pSkill < 33)
				difficulty = EASY;
			else if (pSkill >= 33 && pSkill <= 66)
				difficulty = MEDIUM;
			else {
				difficulty = HARD;
			}
			return scoreChange;
			
		}

		public int getTotalScore(){
			return scoreTotal;
		}

		public int getDifficulty(){
			return difficulty;
		}

		public int getRoundScore(){
			return scoreRound;
		}

		public int getMultiplier(){
			return difficulty + 1;
		}
	}


using UnityEngine;
using System.Collections;

public class SceneImageController : MonoBehaviour {

	// access to the scene controller
	public UIRoot rootObject;

	private SceneController scnController;

	private const int sad = 0;
	private const int happy = 1;
	private const int content = 2;
	private const int angry = 3;

	private int numEmotions = 4;
	private int curEmotion;

	System.Random rand = new System.Random();

	// Use this for initialization
	void Start () {
		scnController = rootObject.GetComponent<SceneController> ();

		pickEmotion ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	// ONLY PUBLIC FOR TESTING PURPOSES (PROBABLY)
	public void pickEmotion() {
		int nextEmotion;

		do {
			nextEmotion = rand.Next (numEmotions);
		} while (nextEmotion == curEmotion);

		curEmotion = nextEmotion;
		displayEmotion (curEmotion);
	}


	void displayEmotion(int i) {
		switch (i) 
		{
		case sad:
			gameObject.GetComponentInChildren<UISprite> ().spriteName = "SadFace";
			gameObject.GetComponentInChildren<UILabel>().text = "sad";
			break;
		case happy:
			gameObject.GetComponentInChildren<UISprite> ().spriteName = "HappyFace";
			gameObject.GetComponentInChildren<UILabel>().text = "happy";
			break;
		case content:
			gameObject.GetComponentInChildren<UISprite> ().spriteName = "ContentFace";
			gameObject.GetComponentInChildren<UILabel>().text = "content";
			break;
		case angry:
			gameObject.GetComponentInChildren<UISprite> ().spriteName = "AngryFace";
			gameObject.GetComponentInChildren<UILabel>().text = "angry";
			break;
		default:
			break;
		}
	}

	public bool isCorrect(string str) {
		string emotion = gameObject.GetComponentInChildren<UILabel> ().text;

		if (emotion.Equals (str)) {
			scnController.resetButtonColors();
			return true;
		}

		return false;
	}
}

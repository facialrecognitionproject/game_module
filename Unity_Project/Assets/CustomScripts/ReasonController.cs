using UnityEngine;
using System.Collections;

public class ReasonController : MonoBehaviour
{
	// All emotions in the game (used to get button choices)
	//TODO: un-hardcode this
	private ArrayList emotionsLst;

	// Difficulty represented as int 0-2 inclusive
	private int difficulty;
	
	// Holds the current modules main emotion
	private string emotion {
		set {
			correctAnswer = value;
			roundDescription.GetComponent<UILabel>().text = correctAnswer;
		}
	}

	// Holds the current round's correct answer
	private string correctAnswer;

	// Reference to all the buttons. If a button is being used, it must be activated
	public GameObject[] buttons;

	// Reference to GameController parent
	public GameObject gameContainer;

	// Reference to the round description object
	public GameObject roundDescription;

	public void checkAnswer(UILabel lbl)
	{
		if (lbl.text == correctAnswer) {
			gameContainer.GetComponent<GameController>().CorrectAnswer();
		} else {
			gameContainer.GetComponent<GameController>().IncorrectAnswer();
		}
	}

	// Set up the level with correct assets
	public void setUpRound(int diff, string emo)
	{
		this.difficulty = diff;
		this.emotion = emo;
	}

	void OnEnable()
	{
		emotionsLst = new ArrayList();
		// Subscribe to event
		SettingsController.OnSubmitSelected += SubmitButtonDelegate;

		emotionsLst.Add("happy");
		emotionsLst.Add("sad");
		emotionsLst.Add("angry");
		emotionsLst.Add("content");
		emotionsLst.Add("surprised");
		
		
		emotionsLst.Remove(correctAnswer);
		
		if (difficulty == 2) {
			setUpButtons(Mathf.Min(5, emotionsLst.Count - 1), emotionsLst); 
		} else {
			setUpButtons(Mathf.Min(3, emotionsLst.Count - 1), emotionsLst); 
		}

	}

	public void SubmitButtonDelegate()
	{
		//TODO implement this method, it is called whenever the settings submit button is clicked
	}

	void OnDisable()
	{
		// Unsubscribe from event
		SettingsController.OnSubmitSelected -= SubmitButtonDelegate;
	}

	private void setDescription()
	{
		//TODO: implement method
	}

	// Choose button answers
	private void setUpButtons(int k, ArrayList remainingEmos)
	{

		System.Random rnd = new System.Random();
		int num = rnd.Next(k + 1);

		for (int i = 0; i <= k; i++) {
			if (num == i) {
				buttons [i].SetActive(true);

				buttons [i].GetComponentInChildren<UILabel>().text = correctAnswer;

			} else {
				int listIndex = rnd.Next(remainingEmos.Count);

				buttons [i].SetActive(true);

				buttons [i].GetComponentInChildren<UILabel>().text = remainingEmos [listIndex].ToString();

				remainingEmos.RemoveAt(listIndex);
			}
		}
	}

}

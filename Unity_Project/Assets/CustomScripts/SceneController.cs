﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void resetButtonColors() {
		UIButton[] buttons = GetComponentsInChildren<UIButton> ();
		for (int i = 0; i < buttons.Length; i++) {
			buttons[i].defaultColor = Color.white;
		}
	}
}

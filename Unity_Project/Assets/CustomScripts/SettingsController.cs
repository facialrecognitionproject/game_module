﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SettingsController : MonoBehaviour
{
	// The subscriber type
	public delegate void SettingsEvent();
	// The event which is fired when the submit button is selected
	public static event SettingsEvent OnSubmitSelected;

	// Constants of indexes of each emotion
	private const int Happy = 0;
	private const int Sad = 1;
	private const int Angry = 2;
	private const int Content = 3;
	private const int Surprised = 4;

	// Array of toggle switches
	public GameObject[] toggle;

	// Popup list gameObject
	public GameObject popupList;

	// Holds the currently active emotions
	// By default, all emotions are active
	private List<string> activeEmos;

	// Holds the current number of rounds
	// By default, 10 rounds will be selected
	private int totRds = 10;
	// Property encapsulating the number of rounds
	public int totalRounds {
		get {
			return totRds;
		}
		private set {
			totRds = value;
		}
	}

	// Use this for initialization
	void Awake()
	{
		activeEmos = new List<string>();

		// By default, all emotions are enabled
		activeEmos.Add("happy");
		activeEmos.Add("sad");
		activeEmos.Add("angry");
		activeEmos.Add("content");
		activeEmos.Add("surprised");
		totalRounds = 10;
	}

	// Called when user clicks submit
	public void submitButtonPressed()
	{
		// Fire the event
		if (OnSubmitSelected != null)
			OnSubmitSelected();
		updateEmotions();
		updateRoundNumber();
	}

	// Return the active emotions
	public string[] getActiveEmotions()
	{
		string[] rtnArr = activeEmos.ToArray();

		return rtnArr;
	}

	public void popupChoiceSelected()
	{
		string curVal = popupList.GetComponent<UIPopupList>().value;

		popupList.GetComponentInChildren<UILabel>().text = "Number of rounds: " + curVal;
	}

	// Updates the list of active emotions
	private void updateEmotions()
	{
		activeEmos.Clear();
		
		foreach (GameObject curToggle in toggle) {
			if (curToggle.GetComponent<UIToggle>().value) {          
				activeEmos.Add(curToggle.transform.FindChild("Label").GetComponent<UILabel>().text.ToLower());
			}
		}
	}

	// Updates the round number
	private void updateRoundNumber()
	{
		totalRounds = int.Parse(popupList.GetComponent<UIPopupList>().value); 
	}

	public bool GetMusicSetting()
	{
		return transform.FindChild("PlayMusicToggle").GetComponent<UIToggle>().value;
	}
}

﻿using UnityEngine;
using System.Collections;

public class SpriteChanger : MonoBehaviour {

	// Use this for initialization
	void Start () {

		UISprite sprite = GetComponent<UISprite> ();
		sprite.spriteName = "Beveled Outline";
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

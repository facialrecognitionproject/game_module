﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackgroundController : MonoBehaviour
{
	// Prefab used for each photo hanging on the wall
	// TODO: prefab for mirror
	public GameObject photoPrefab;
	public Camera mainCam;
	
	// Number of rounds correlates to number of photos hanging
	private int numberRounds = 5;
	// Will hold height and width of the camera bounds
	private float height, width = 0f;
	
	// Holds the positions of each photo
	private LinkedList<Vector3> positions = new LinkedList<Vector3>();
	private LinkedList<Object> objs = new LinkedList<Object>();

	// References to position of current photo and next photo
	private	LinkedListNode<Vector3> curNode, nextNode;
	
	// Random number generator
	private System.Random rnd = new System.Random();
	
	// Keeps reference to the z position of the camera obtained at runtime
	// (might not need this, idk)
	private float cameraZ;
	
	// Time for testing purposes
	public int time = 0;
	
	// Holds this match's emotions
	private string[] theseEmotions = null;
	bool moving = false;
	// Update is called once per frame
	void Update()
	{

		// Linearly interprolate through the distance between two photos if we are moving levels
		if (moving) {
			// The percantage along the distance of the interprolation
			float fracJourney = (float)((Time.time - startTime) / 1.0f);
			// Move photo to correct position
			mainCam.transform.position = Vector3.Lerp(curCameraPosition, nextCameraPosition, fracJourney);
			
			// Moving should stop once the camera moved to the right spot
			if (fracJourney > 1.0)
				moving = false;
		}

	}
	
	void OnEnable()
	{
		SettingsController.OnSubmitSelected += SettingsMenuSubscriber;
		GameController.OnStartSession += PositionSessionPictures;
		GameController.OnNextRound += NextRound;
		GameController.OnEndSession += Reset;

	}

	void OnDisable()
	{
		SettingsController.OnSubmitSelected -= SettingsMenuSubscriber;
		GameController.OnStartSession -= PositionSessionPictures;
		GameController.OnNextRound -= NextRound;
		GameController.OnEndSession -= Reset;
	}

	public void SettingsMenuSubscriber()
	{
		MenuSystemController menuController = GameObject.FindGameObjectWithTag("MenuController")
			.GetComponent<MenuSystemController>();

		theseEmotions = menuController.settingsScreen.getActiveEmotions();
		numberRounds = menuController.settingsScreen.totalRounds;

		Debug.Log("Updated rounds to be: " + numberRounds);
		string emos = "Using the following emotions:";
		foreach (string str in theseEmotions)
			emos += "," + str;
		Debug.Log(emos);
	}
	
	// Used to linearly interprolate the distance between the current and next photo
	float startTime;
	float journeyLength;
	// Holds the positions for the camera
	Vector3 curCameraPosition;
	Vector3 nextCameraPosition;
	
	// Updates to next round
	public void NextRound()
	{
		if (curNode == null) {
			// Set up the current node and the next node
			curNode = positions.First;
			nextNode = curNode.Next;
			
			// Set up the initial camera location to the first picture
			// Camera position is photoPosition with correct z value
			Vector3 photoPosition = curNode.Value;
			Vector3 cameraPosition = new Vector3(photoPosition.x, photoPosition.y - 0.6f, cameraZ);
			mainCam.transform.position = cameraPosition;
		} else {
			// Only move to the next round if there is another position remaining
			if (nextNode != null) {
				moving = true;
				// Camera position is photoPosition with correct z value
				curCameraPosition = new Vector3(curNode.Value.x, curNode.Value.y - 0.6f, cameraZ);
				nextCameraPosition = new Vector3(nextNode.Value.x, nextNode.Value.y - 0.6f, cameraZ);
				journeyLength = (nextCameraPosition - curCameraPosition).magnitude;
				startTime = Time.time;
			
				curNode = nextNode;
				nextNode = curNode.Next;
			}
		}
	}

	private Sprite getSpriteImageForImage(string imgName)
	{
		switch (imgName) {
			case "happy":
				return ImageLoader.GetRandomFace(ImageLoader.EmotionType.HAPPY);
			case "sad":
				return ImageLoader.GetRandomFace(ImageLoader.EmotionType.SAD);
			case "angry":
				return ImageLoader.GetRandomFace(ImageLoader.EmotionType.ANGRY);
			case "surprised":
				return ImageLoader.GetRandomFace(ImageLoader.EmotionType.SURPRISED);
			case "content":
				return ImageLoader.GetRandomFace(ImageLoader.EmotionType.CONTENT);
		}
		return null;
	}

	public void PositionSessionPictures()
	{
		MenuSystemController menuController = GameObject.FindGameObjectWithTag("MenuController")
			.GetComponent<MenuSystemController>();

		GameController gameController = GameObject.FindGameObjectWithTag("GameController")
			.GetComponent<GameController>();
		
		theseEmotions = gameController.thisSessionEmotions;

		numberRounds = menuController.settingsScreen.totalRounds;
		// Get the height, width, and z position of the camera
		height = 2f * mainCam.orthographicSize;
		width = height * mainCam.aspect;
		cameraZ = mainCam.transform.position.z;
		// Current x position for the photo
		float xPosition = 0f;
		// For each round, we will create a photo out of the prefab object
		// Each frame will be placed a camera width away and a y position of +/- .2 times the height
		// Add each position to the linked list for the camera to follow
		for (int p = 0; p < numberRounds; p++) {
			// Select the photo that will be displayed in current frame
			// Create position with current x position and random y position
			float y = (float)(rnd.NextDouble() * 0.4 - 0.2) * height;
			Vector3 pos = new Vector3(xPosition, y);
			GameObject obj = Instantiate(photoPrefab, pos, Quaternion.identity) as GameObject;
			obj.transform.parent = this.transform;
			if (theseEmotions != null) {
				SpriteRenderer spriteRenderer = obj.GetComponent<SpriteRenderer>();
				spriteRenderer.sprite = getSpriteImageForImage(theseEmotions [p]);
			}

			// Put the object on to the scene at the correct position
			objs.AddLast(obj);
			// Add the position to the list and update the xPosition
			positions.AddLast(pos);
			xPosition += width;
		}
	}

	public void Reset()
	{
		foreach (GameObject obj in objs)
			DestroyObject(obj);

		objs.Clear();
		positions.Clear();
		curNode = null;
		nextNode = null;
	}
}
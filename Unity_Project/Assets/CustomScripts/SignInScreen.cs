﻿using UnityEngine;
using System.Collections;

public class SignInScreen : MonoBehaviour
{

	public void SetPlayerName ()
	{
		GameObject obj = GameObject.FindGameObjectWithTag ("Player");
		Player player = obj.GetComponent<Player> ();
		Transform inputchild = transform.FindChild ("NameField");
		UIInput inputName = inputchild.GetComponent<UIInput> ();
		player.playerName = inputName.value;
	}
}

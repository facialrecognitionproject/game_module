using UnityEngine;
using System.Collections;

/**
 * This class simply acts as controller to hold various functions which give the main menu buttons various functions.
 * 
 * @author Andrew Daniel
 */
public class MainMenuScreen : MonoBehaviour
{

	public delegate void EventHandler();

	public static event EventHandler OnPlayButtonPressed;
	public static event EventHandler OnSettingsButtonPressed;

	// Quits the game
	public void QuitGame()
	{
		Application.Quit();
	}

	// Loads the settings menu
	public void LoadSettingsMenu()
	{
		if (OnSettingsButtonPressed != null)
			OnSettingsButtonPressed();
	}

	// Starts the game
	public void PlayGame()
	{
		if (OnPlayButtonPressed != null)
			OnPlayButtonPressed();
	}

}

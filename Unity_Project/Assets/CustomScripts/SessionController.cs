﻿using UnityEngine;
using System.Collections;

public class SessionController : MonoBehaviour
{
	// The player
	public Player player;
	// The function signature for a subscriber to the SessionControllers events
	public delegate void SessionEvent();
	// Events that can be subscribed to
	public static event SessionEvent OnGameStarted;
	public static event SessionEvent OnGameEnded;
	// Called when user presses start game
	public void StartGame()
	{
		if (OnGameStarted != null)
			OnGameStarted();
		GameObject.FindGameObjectWithTag("GameController").SetActive(true);
		GameObject.FindGameObjectWithTag("MenuController").SetActive(false);
	}

	public void EndGame()
	{
		if (OnGameEnded != null)
			OnGameEnded();
		GameObject.FindGameObjectWithTag("GameController").SetActive(false);
		GameObject.FindGameObjectWithTag("MenuController").SetActive(true);
	}
}

﻿using UnityEngine;
using System.Collections;

public class WebCamVideoFeed : MonoBehaviour
{

	WebCamTexture webTex;

	void Awake ()
	{
		webTex = new WebCamTexture ();
		this.GetComponent<Renderer> ().material.mainTexture = webTex;
	}

	void OnEnable ()
	{
		webTex.Play ();
	}

	void OnDisable ()
	{
		webTex.Stop ();
	}
}

﻿using UnityEngine;
using System.Collections;

public class GameOverController : MonoBehaviour {

	public GameObject scoreLabel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setFinalScore(int score) {
		scoreLabel.GetComponent<UILabel>().text = "Final Score: " + score;
	}

}

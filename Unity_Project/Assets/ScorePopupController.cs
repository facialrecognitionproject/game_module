﻿using UnityEngine;
using System.Collections;

public class ScorePopupController : MonoBehaviour {

	public int destroyTime = 2;
	public double maxXMovement = 2.0;
	public double maxYMovement = 1.0;
	
	System.Random rnd = new System.Random();

	// Use this for initialization
	void Start () {
		double posX = gameObject.transform.position.x + (rnd.NextDouble() % (maxXMovement * 2.0)) - maxXMovement;
		double posY = gameObject.transform.position.x + (rnd.NextDouble() % (maxYMovement * 2.0)) - maxYMovement;

		gameObject.transform.position = new Vector3((float)posX, (float)posY);

		StartCoroutine(startAnimation());
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.localScale = gameObject.transform.localScale + new Vector3(0.05f, 0.05f, 0.0f);
	}

	IEnumerator startAnimation() {
		yield return new WaitForSeconds(destroyTime);
		Destroy(this.gameObject);
	}
}
